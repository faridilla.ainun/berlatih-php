<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>SOAL 1</h1>
    <?php
        function tentukan_nilai ($number)
        {
            $nilai = $number;
            $output = "";
            if ($nilai>=85)
                {
                $output = "Sangat baik"; 
                }else if($nilai<85 && $nilai>=70){
                $output = "Baik";
                }else if($nilai<70 && $nilai>=60){
                $output = "Cukup";
                }else{
                $output = "Kurang";
                }
                return $output . "<br>";
            }


        // TEST CASES
        echo tentukan_nilai(98); //Sangat Baik
        echo tentukan_nilai(76); //Baik
        echo tentukan_nilai(67); //Cukup
        echo tentukan_nilai(43); //Kurang
    ?>
</body>
</html>